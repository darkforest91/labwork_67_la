<?php

$student_surname = [];
$student_growth = [];

for ($i = 1; $i <= 3; $i++) {
    print "Назовите фамилию {$i}-го студента: ";
    $student_surname[] = trim(fgets(STDIN));
    print "Какой рост у {$i}-го студента: ";
    $student_growth[] = trim(fgets(STDIN));
}

$highest_student = max($student_growth);

for ($i = 0; $i < count($student_growth); $i++) {
    if ($student_growth[$i] == $highest_student) {
        print "Фамилия самого высокого студента {$student_surname[$i]}. Его рост {$highest_student}.";
    }
}