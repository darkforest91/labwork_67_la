<?php

$string_1 = 'Programmer';
$string_2 = 'Progression';
$string_3 = 'Programmer';

$option_a = searchesInitialCharacterMatches($string_1, $string_2);
print "Вариант а) -> Совпадений: {$option_a}.\n";

$option_b = searchesInitialCharacterMatches($string_1, $string_3);
print "Вариант б) -> Совпадений: {$option_b}.";

function searchesInitialCharacterMatches($string_1, $string_2) {
    $count_matches = 0;
    for ($i = 0; $i < strlen($string_1); $i++) {
        if ($string_1[$i] === $string_2[$i]) {
            $count_matches = $count_matches + 1;
        }
    }
    return $count_matches;
}