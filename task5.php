<?php

print "Введите число А: ";
$number_a = trim(fgets(STDIN));

print "Введите натуральное число N: ";
$number_n = trim(fgets(STDIN));

$result = 0;
for ($i = 1; $i <= $number_n; $i++) {
    $result = $i * $number_a + $result;
}

print "Результат: " . $result;