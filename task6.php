<?php

$array = [];
print "Заполните в массив 5 чисел.\n";

for ($i = 1; $i <= 5; $i++) {
    print "Введите {$i}-е число: ";
    $array[] = trim(fgets(STDIN));
}

$difference = [];
for ($i = 1; $i < count($array); $i++) {
    $difference[] = $array[$i] - $array[$i-1];
}

$result = array_unique($difference);

if (count($result) == 1) {
    print "Расзность прогрессии = {$result[0]}";
} else {
    print "null";
}