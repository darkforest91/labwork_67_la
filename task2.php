<?php

$scored_balls = [];
$missed_balls = [];

for ($i = 1; $i <= 20; $i++) {
    $scored_balls[] = rand(1, 12);
    $missed_balls[] = rand(1, 12);
}

for ($i = 0; $i < count($scored_balls); $i++) {
    if ($scored_balls[$i] > $missed_balls[$i]) {
        print $i + 1 . ". " . "Выигрыш. Счет {$scored_balls[$i]}:{$missed_balls[$i]}\n";
    }
    elseif ($scored_balls[$i] < $missed_balls[$i]) {
        print $i + 1 . ". " . "Проигрыш. Счет {$scored_balls[$i]}:{$missed_balls[$i]}\n";
    }
    else {
        print $i + 1 . ". " . "Ничья. Счет {$scored_balls[$i]}:{$missed_balls[$i]}\n";
    }
}